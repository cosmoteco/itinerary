<?php

namespace Src\Exception;

class BadRequestException extends \Exception
{
    const CODE = 400;
}