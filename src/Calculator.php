<?php

namespace Src;

use Src\Exception\BadRequestException;

class Calculator
{
    /**
     * @param array $routes
     *
     * @return string
     * @throws BadRequestException
     */
    public function sortRoutes(array $routes): string
    {
        $start = $this->findItineraryStart($routes);

        $sortedRoutes = [];
        while (isset($routes[$start])) {
            $sortedRoutes[$start.'@'.$routes[$start]] = $routes[$start.'@'.$routes[$start]];
            $start = $routes[$start];
        }

        if (count($sortedRoutes) !== count($routes)) {
            throw new BadRequestException(
                sprintf("Can't determine the itinerary by %s starting point. Check that one ticket departure matches with another's destination!", $start),
                400
            );
        }

        return json_encode($sortedRoutes);
    }

    /**
     * @param array $routes
     * @return string
     */
    protected function findItineraryStart(array $routes): string
    {
        $flippedTickets = array_flip($routes);

        $diff = array_diff($flippedTickets,$routes);

        return array_shift($diff);
    }
}