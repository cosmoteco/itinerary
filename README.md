# README #

### What is this repository for? ###

* Gets an array with unsorted "from to sequences" and returns the itinerary, from start to end, as array.

### How do I get set up? ###

* Include the librart by composer. Add "cosmoteco/itinerary": "dev-master" to require section.
* Configuration
* Dependencies: JSON, PHP version >= 7.1.3

### Contribution guidelines ###

* Writing tests
* Code review

### Example of input and output. 

Input:
```php
// Anton is the start of the trip.
// It's mandatory to set the start of a route the same as the previous routes's destination in order to determine the complete itinerary.
[
    "Insbruck Airport" => "Venice",
    "San Ruff"         => "Marconi",
    "CDG"              => "Chicago",
    "Marconi"          => "CDG",
    "Venice"           => "San Ruff",
    "Hbf"              => "Insbruck Airport",
    "Anton"            => "Hbf"
];
```
Success Output:
```json
{
    "Anton":"Hbf",
    "Hbf":"Insbruck Airport",
    "Insbruck Airport":"Venice",
    "Venice":"San Ruff",
    "San Ruff":"Marconi",
    "Marconi":"CDG",
    "CDG":"Chicago"
}
```
Error output:
```json
{
"error":"Can't determine the itinerary by Parisss starting point.",
"code":400
}
```

### Example of usage once the library was installed.
```php
$routes = [
    "Insbruck Airport" => "Venice",
    "San Ruff"         => "Marconi",
    "CDG"              => "Chicago",
    "Marconi"          => "CDG",
    "Venice"           => "San Ruff",
    "Hbf"              => "Insbruck Airport",
    "Anton"            => "Hbf"
];

$itinerarySorter = new Calculator();
/** @var string $sortedRoutes */
$sortedRoutes = $itinerarySorter->sort($routes);
```
